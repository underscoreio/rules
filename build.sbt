name := "rules"

scalaVersion := "2.11.1"

scalacOptions ++= Seq(
  "-deprecation", 
  "-encoding", "UTF-8",
  "-feature"
)

resolvers += "JBoss repository" at "https://repository.jboss.org/nexus/content/groups/public-jboss/"

libraryDependencies ++= {
  val droolsVersion = "6.0.1.Final"
  Seq(
    "org.drools" % "drools-core" % droolsVersion,
    "org.drools" % "drools-compiler" % droolsVersion
 )
}

// Settings just for the Eclipse SBT plug-in

EclipseKeys.withSource := true

EclipseKeys.createSrc := EclipseCreateSrc.Default + EclipseCreateSrc.Resource

