# Drools Demo


## How to run it:

    $ sbt run
    
## What it outputs:

	Fired 1 rules
	When Hungry: List(Outcome(Eat some food))
	
	Fired 1 rules
	When Thirsty: List(Outcome(Have a drink))
	
	Fired 0 rules
	When Thirsty and Drunk: List()
	
	Fired 2 rules
	When Hungry and Thirsty: List(Outcome(Eat some food), Outcome(Have a drink))
	
	Fired 1 rules
	When Hungry and only a little Thirsty: List(Outcome(Eat some food))
	
## What was that?

The following set of rules (_src/main/resources/rules/rules.drl_) ...

	rule "Hungry Rule"
	  salience 0
	  when
	    Hunger(value > 10)
	  then
	    insert( new Outcome("Eat some food") )
	end
	
	rule "Thirsty Rule"
	  salience 0
	  when
	    Thirst(value > 10) and
	    not(Drunk(value == true))
	  then
	    insert( new Outcome("Have a drink") )
	end
	
...as loaded from the classpath, and evaluate against different sets of "facts" in the _Main_.

Facts are just Java Beans.

## But...

* Salience is a weighting or priority, to make some rules fire earlier.
* When a rule fires it can insert some other fact(s) into the system.  We are querying for facts of a particular type (`Outcome`).
* You can fire all the rules (as we did here) or just the best match rule.
* You can insert other facts into the system, and run it until it settles, if you want.
* The main reason for using Drools is that it has a good indexing system for evaluating rules efficiently.  If you have complex rules and/or lots of rules, this is much much better than the naive implementation you might try.
* The expression language is quite rich
* You can have expressions in rules that call code (e.g., do some computation rather than just inspect a value)






