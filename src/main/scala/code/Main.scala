package code

object Main extends App {


  println( "When Hungry: "+RuleEngine.evaluate(Hunger(100)) )

  println( "When Thirsty: "+RuleEngine.evaluate(Thirst(100)) )

  println( "When Thirsty and Drunk: "+RuleEngine.evaluate(Thirst(100), Drunk(true)) )

  println( "When Hungry and Thirsty: "+RuleEngine.evaluate(Thirst(100), Hunger(100)) )

  println( "When Hungry and only a little Thirsty: "+RuleEngine.evaluate(Thirst(1), Hunger(100)) )


}
