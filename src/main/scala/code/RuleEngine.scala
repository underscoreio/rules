package code

import org.kie.internal.builder.KnowledgeBuilderFactory
import org.kie.internal.io.ResourceFactory
import org.kie.api.io.ResourceType
import org.kie.internal.KnowledgeBaseFactory

import scala.collection.JavaConverters._

object RuleEngine {

  // 1. Compile the rules:
  val builder =  KnowledgeBuilderFactory.newKnowledgeBuilder()
  builder.add(ResourceFactory.newClassPathResource("rules/rules.drl"), ResourceType.DRL)
  builder.add(ResourceFactory.newClassPathResource("rules/query.rule"), ResourceType.DRL)

  // 2. Set up a knowledge base
  val conf = KnowledgeBaseFactory.newKnowledgeBaseConfiguration()
  val kBase = KnowledgeBaseFactory.newKnowledgeBase(conf)

  // 3. Add the rules to the knowledge base:
  kBase.addKnowledgePackages(builder.getKnowledgePackages)


  // 4. Run the rules against some facts (which we have no constrained to be a partucular type)
  def evaluate(facts: AnyRef*): Seq[Outcome] = {

    // 4a Get a session. There is a stateless one too.  Not sure how to use it.
    val session = kBase.newStatefulKnowledgeSession()

    // 4b. Insert the facts:
    facts.foreach(session.insert)

    // 4c. Rule those rules!
    val firedCount = session.fireAllRules()
    // session.fireAllRules(1) to get the one best matching rule.

    println(s"Fired $firedCount rules")

    // 4d. Pluck out the outcome(s)
    val results = for {
      results <- Option(session getQueryResults "outcomes").toSeq
      row     <- results.iterator().asScala
      value   <- Option(row get "outcome")
    } yield value

    session.dispose()

    results.collect { case o : Outcome => o }

  }

}