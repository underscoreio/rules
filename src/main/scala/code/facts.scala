package code

import scala.beans.BeanProperty

@BeanProperty
case class Thirst(value: Int)

@BeanProperty
case class Hunger(value: Int)

@BeanProperty
case class Drunk(value: Boolean)


@BeanProperty
case class Outcome(message: String)

